# Projeto Integrador - Análise dos dados do COVID-19

Este trabalho tem o objetivo de entender os fatores que levaram ao recuo da fase verde para a fase amarela como medida protetiva do avanço da pandemia de COVID-19 no estado de São Paulo. 

Para isso, nós relacionamos as informações do número de casos confirmados e de óbitos por Covid-19 no Estado de São Paulo, com os dados de mobilidade urbana e os relatórios mensais do plano São Paulo, para ver o quão efetivo estão sendo essas medidas de prevenção.

